import javax.swing.*;
import java.awt.*;
/*
uses the NumberEngine class to create boards for the players
sets each players board with a jpanel holding the cells for each grid
grids can be accessed/iterated through using playerList[player].board[row][column]
*/
@SuppressWarnings("serial")
public class NewBoardPanel extends JPanel{

    private Cell[][] cellHolder;      //reference to all the cells on the board, useful for manipulating them
    final private int ROWS_ON_BOARD    =  5; //15 cols
    final private int COLUMNS_ON_BOARD =  5; //5 rows

    public NewBoardPanel(int[] boardLayout){
        //index tracks the current position in the boardLayout, which is the random numbers generated then fed to the constructor
        int index  =  0;     

        //set layout of the JPanel
        this.cellHolder = new Cell[ROWS_ON_BOARD][COLUMNS_ON_BOARD];    
        setLayout(new GridLayout(ROWS_ON_BOARD,COLUMNS_ON_BOARD));

        //populate JPanel with Cells (subclass of JLabel that generate image using constructor
        System.out.println("NewBoardPanel being populated");
        for(int row = 0; row < ROWS_ON_BOARD; row++) {
            for(int col = 0; col < COLUMNS_ON_BOARD; col++){
                //System.out.println("Placing value " + boardLayout[index] + " in row " + row + ", col " + col);
                this.cellHolder[row][col] = new Cell(boardLayout[index]);
                add(cellHolder[row][col]);
                index++;
            }
        }
        //sets free space on the board
        this.cellHolder[2][2].setFreeSpace();
    }

    public void setFreeSpace(){
        this.cellHolder[2][2].setFreeSpace();
    }

    //take x, y coord of point on board & check respective Cell() as to whether or not it has been selected
    public int _check(int row, int col){
        return (this.cellHolder[row][col].selected == true) ? 1 : 0;
    }

    //checks the board for win status: board win == true if any row, column, or cross are all selected
    public boolean winStatus(){
        boolean boardWinStatus = false;

        //check for wins in any of the rows
        if (_check(0,0) + _check(0,1) + _check(0,2) + _check(0,3) + _check(0,4) == 5) boardWinStatus = true;
        if (_check(1,0) + _check(1,1) + _check(1,2) + _check(1,3) + _check(1,4) == 5) boardWinStatus = true;
        if (_check(2,0) + _check(2,1) + _check(2,2) + _check(2,3) + _check(2,4) == 5) boardWinStatus = true;
        if (_check(3,0) + _check(3,1) + _check(3,2) + _check(3,3) + _check(3,4) == 5) boardWinStatus = true;
        if (_check(4,0) + _check(4,1) + _check(4,2) + _check(4,3) + _check(4,4) == 5) boardWinStatus = true;

        //check for wins in any of the col
        if (_check(0,0) + _check(1,0) + _check(2,0) + _check(3,0) + _check(4,0) == 5) boardWinStatus = true;
        if (_check(0,1) + _check(1,1) + _check(2,1) + _check(3,1) + _check(4,1) == 5) boardWinStatus = true;
        if (_check(0,2) + _check(1,2) + _check(2,2) + _check(3,2) + _check(4,2) == 5) boardWinStatus = true;
        if (_check(0,3) + _check(1,3) + _check(2,3) + _check(3,3) + _check(4,3) == 5) boardWinStatus = true;
        if (_check(0,4) + _check(1,4) + _check(2,4) + _check(3,4) + _check(4,4) == 5) boardWinStatus = true;

        //check either cross for winning condition
        if (_check(0,0) + _check(1,1) + _check(2,2) + _check(3,3) + _check(4,4) == 5) boardWinStatus = true;
        if (_check(0,4) + _check(1,2) + _check(2,2) + _check(3,1) + _check(4,0) == 5) boardWinStatus = true;

        return boardWinStatus;
    }

    //newBoard will refresh board by accepting an array of numbers to reassign cells with
    public void newBoard(int[] newNumbers){
        int index = 0;
        for(Cell[] array: this.cellHolder){
            for(Cell c: array){
                c.changeCell(newNumbers[index]);
                index++;
            }
        }
    }

    //when a number is called, is checked against the cells of the board
    public void checkBoard(int val){
        for(Cell[] array: this.cellHolder){
            for(Cell c: array){
                c.checkCell(val);
            }
        }
    }
}
