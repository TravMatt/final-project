import javax.swing.*;
import java.awt.*;    //Needed for BorderLayout Classes
import java.util.*;
import java.util.Random;


public class BingoModel{

    //players name 
    public String userName;

    //playerlist holds the player objects
    private Player[] playerList = new Player[4];    

    //holds the array of random numbers which will be called as game is stepped through
    int[] numbersToCall;

    //trackTurn tracks the current step in the game
    int trackTurn;
 
    //track the current number to be called
    int numberCalled;

    //tracks the winners
    ArrayList<String> winners;

    //constructor
    public BingoModel(String userName){
        //System.out.println("BingoModel constructor called");

        //create players and add to array
        this.playerList[0] = new Player();
        this.playerList[1] = new Player();
        this.playerList[2] = new Player();
        this.playerList[3] = new Player();

        //set player names
        this.playerList[0].setName(userName);
        this.playerList[1].setName();
        this.playerList[2].setName();
        this.playerList[3].setName();
        
        //set player boards
        int[] userNumbers = NumberEngine.genBoard();
        int[] npc1Numbers = NumberEngine.genBoard();
        int[] npc2Numbers = NumberEngine.genBoard();
        int[] npc3Numbers = NumberEngine.genBoard();

        NewBoardPanel userBoard = new NewBoardPanel(userNumbers);
        NewBoardPanel npc1Board = new NewBoardPanel(npc1Numbers);
        NewBoardPanel npc2Board = new NewBoardPanel(npc2Numbers);
        NewBoardPanel npc3Board = new NewBoardPanel(npc3Numbers);

        this.playerList[0].setBoard(userBoard);
        this.playerList[1].setBoard(npc1Board);
        this.playerList[2].setBoard(npc2Board);
        this.playerList[3].setBoard(npc3Board);

        //generate all the numbers which will be called
        this.numbersToCall = NumberEngine.genNums();

        //set turn to 0
        this.trackTurn = 0;

        //reset winners
        this.winners = new ArrayList<String>(4);

        //set board visible
        System.out.println("Setting player board visible");
        this.playerList[0].board.setVisible(true);
    }

    //public NewBoardPanel getGrid(){
    //    return playerList[0].board;
    //}

    public NewBoardPanel getUserBoard(){
        return playerList[0].board;
    }

    public NewBoardPanel getNPC1Board(){
        return playerList[1].board;
    }

    public NewBoardPanel getNPC2Board(){
        return playerList[2].board;
    }

    public NewBoardPanel getNPC3Board(){
        return playerList[3].board;
    }

    public void getNewBoards(){
        //System.out.println("model.getNewBoards() called");
        for(Player p: playerList){
            p.setNewBoard(NumberEngine.genBoard());
            p.setFreeSpace();
        }
        this.winners = new ArrayList<String>();
        this.numbersToCall = NumberEngine.genNums();        
        this.trackTurn = 0;
    }

    public boolean _callNumber(){
        boolean _winnerFound = false;
        this.numberCalled = numbersToCall[trackTurn];
        this.trackTurn++;

        for(Player p: playerList){
            if(p.checkNumberCalled(this.numberCalled) == true){
                this.winners.add(p.name);
            }
        }
        if(this.winners.size() > 0){
            _winnerFound = true;
        }
        return _winnerFound;
    }


    static class NumberEngine{

        /*This Class controls the generation of random numbers, suitable for creation of game boards and the iteration of the game*/
        //Class is static because there should be no need to instantiate, will be returning similar values every time

        /*The numbers in the columns of a Bingo ticket are selected at random and printed according to the range:
         *B column: are from 1 to 15
         *I column: 16 and 30
         *N column (that contains four numbers and the free (blank) space) between 31 and 45
         *G column between 46 and 60
         *O column: 61 and 75 */
        static int[] B = {1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15};
        static int[] I = {16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30};
        static int[] N = {31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45};
        static int[] G = {46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60};
        static int[] O = {61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75};
        static int[] completeNumbers = {1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15,
                                        16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,  
                                        31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45,
                                        46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 
                                        61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75};


        // Implementing Fisher–Yates shuffle
        static void shuffleArray(int[] ar){
            Random rnd = new Random();
            for (int i = ar.length - 1; i > 0; i--){
                int index = rnd.nextInt(i + 1);
                // Simple swap
                int a = ar[index];
                ar[index] = ar[i];
                ar[i] = a;
            }
        }

        //a method to concatenate arrays together
        private static int[] concat(int[] arr1, int[] arr2, int[] arr3, int[] arr4, int[] arr5){
            int[] result = new int[25];
            System.arraycopy(arr1, 0, result,  0, 5);
            System.arraycopy(arr2, 0, result,  5, 5);
            System.arraycopy(arr3, 0, result, 10, 5);
            System.arraycopy(arr4, 0, result, 15, 5);
            System.arraycopy(arr5, 0, result, 20, 5);

            return result;
        }
            
        //takes & shuffles each respective column, concatenates all columns into one array & returns said array
        public static int[] genBoard(){
            //return value
            int[] randomized = new int[75];

            //System.out.println("genBoard being called");
            //Generate new columns of numbers
            int[] colB = B; 
            int[] colI = I;
            int[] colN = N;
            int[] colG = G;
            int[] colO = O;

            shuffleArray(colB);
            shuffleArray(colI);
            shuffleArray(colN);
            shuffleArray(colG);
            shuffleArray(colO);

            //since gridLayout adds cells sequentially (i.e., filling second row after first is full
            //this returns #'s in appropriate order: first of every column, second..
            for (int i = 0, j = 0; j < randomized.length; ++i) {
                    randomized[j++] = colB[i];
                    randomized[j++] = colI[i];
                    randomized[j++] = colN[i];
                    randomized[j++] = colG[i];
                    randomized[j++] = colO[i];
            }           

            return randomized;
        }

        //generates the numbers necessary to iter through game 
        public static int[] genNums(){
            //System.out.println("genNums being called");
            int[] numbers = completeNumbers;
            shuffleArray(numbers);
            return numbers;
        } 

        //testing the class
        public static void main(String[] args){
            System.out.println("calling genNums(): " + Arrays.toString( genNums() ));
            System.out.println("calling genBoard(): " + Arrays.toString( genBoard() ));

        }

        //only use to is make code in .genBoard() more terse: takes the shuffled array, returns the first 5 numbers
        public static int[] portion(int[] array){
            return Arrays.copyOfRange(array, 0, 5);
        }
    }//END NumberEngine class
}
