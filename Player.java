import javax.swing.*;

//Abstract player class implements fields and methods necessary to create player objects: to be subclassed by User & NonPlayerCharacter Classes
public class Player{

    //Instance field player count to be used by NPC classes in order to set their names
    private static int playerCount = 1;
    public String name;
    public NewBoardPanel board;

    //Constructor for the Player, NonPlayerCharacter subclasses
    public Player(){
        System.out.println("Player being instantiated");
    }

    //method to set object name must be implemented, either passed (if Player = user) or generated automatically
    public void setName(){
        this.name = "NPC" + Integer.toString(playerCount);
        playerCount++;
    }

    public void setName(String name){
        this.name = name;
    }

    //getter and setter methods for board instance variable
    public void setBoard(NewBoardPanel board){
        this.board = board;
    }

    public void setFreeSpace(){
        this.board.setFreeSpace();
    }

    public JPanel getBoard(JPanel board){
        return this.board;
    }

    //setter method: it was either this or make playerCount public!
    public static void setPlayerCount(){
        playerCount++;
    }

    //getter method: it was either this or make playerCount public!
    public static int getPlayerCount(){
        return playerCount;
    }

    public void setNewBoard(int[] newNumbers){
        board.newBoard(newNumbers);
    }

    public boolean checkNumberCalled(int val){
        boolean _winningBoard;
        board.checkBoard(val);
        if(board.winStatus() == true){
            _winningBoard = true;
        }
        else{
            _winningBoard = false;
        }
            return _winningBoard;
    }
}   
