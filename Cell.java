import javax.swing.*;

//The Cell class is responsible for the images on the board; the actual numbers that make up each board
@SuppressWarnings("serial")
public class Cell extends JLabel{

    //file, fileStub both used by the .changeCell() method to choose another jpg image, once cell value has been selected
    boolean selected = false;

    //val is used by the model to check if the number contained by the cell is the number called
    int val;

    //constructor
    public Cell(int val){
        //call super constructor
        super(Integer.toString(val));
        this.val = val;
    }

    //checkCell will change the cell if it is passed a value == to the value that the cell holds
    public void checkCell(int val){
        if(this.val == val){
            this.selected = true;
            this.setText("X");
        }
    }

    //changeCell will update the grid with new cells, used by newBoardPanel to update the cells
    public void changeCell(int val){
        this.selected = false;
        this.val      = val;
        this.setText(Integer.toString(val));
    }

    //set the free space: middle of the board, will be counted as a marked space
    public void setFreeSpace(){
        this.selected = true;
        this.setText("X");
        this.val      = 0;
    }
}
