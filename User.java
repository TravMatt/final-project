import javax.swing.*;

//Abstract player class implements fields and methods necessary to create player objects: to be subclassed by User & NonPlayerCharacter Classes
public class User extends Player{

    //Constructor for the Player, NonPlayerCharacter subclasses
    public User(){
        super();
    }

    //method to set object name must be implemented, either passed (if Player = user) or generated automatically
    //since User.setName() takes an argument, this should not be called
    public void setName(){
        ;
    }

    //Set user name
    public void setName(String name){
        this.name = name;
    }

}   
