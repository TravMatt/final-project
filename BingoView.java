import javax.swing.*; //Needed for Swing Classes
import java.awt.*;    //Needed for BorderLayout Classes
import java.awt.event.*;
@SuppressWarnings("serial")
public class BingoView extends JFrame{

    private final int WINDOW_WIDTH  = 400;
    private final int WINDOW_HEIGHT = 300;
    private JPanel top;
    private NewBoardPanel center;
    private JPanel  bottom;
    private JButton npc1; 
    private JButton npc2; 
    private JButton npc3; 
    private JButton _step;
    private JButton _new;

    //Constructor
    //public BingoView(BingoModel model, NewBoardPanel center){
    public BingoView(BingoModel model){

        System.out.println("BingoView Constructor called");


        //set frame defaults
        setTitle("Border Layout");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        //create Top, bottom panels
        //Set Top grid== "B I N G O"
        //Set Bottom == [NPC 1][NPC 2][NPC 3][Step][New Game]
        this.top    = createTop();
        this.center = model.getUserBoard();
        this.center.setPreferredSize(new Dimension(500,500));
        this.bottom = createBottom();

        

        //add panels to pane
        add(this.top, BorderLayout.NORTH);
        add(this.center, BorderLayout.CENTER);
        add(this.bottom, BorderLayout.SOUTH);
        pack();
    }

    public JPanel createTop(){
        JPanel top    = new JPanel(new GridLayout(1,5));
        JButton button1 = new JButton("B");
        JButton button2 = new JButton("I");
        JButton button3 = new JButton("N");
        JButton button4 = new JButton("G");
        JButton button5 = new JButton("O");
        top.add(button1);
        top.add(button2);
        top.add(button3);
        top.add(button4);
        top.add(button5);
        return top;
    }

    public JPanel createBottom(){
        JPanel panel = new JPanel(new GridLayout(1,5));

        this.npc1 = new JButton("NPC 1");
        this.npc2 = new JButton("NPC 2");
        this.npc3 = new JButton("NPC 3");
        this._step = new JButton("Step");
        this._new = new JButton("New Game");

        panel.add(npc1);
        panel.add(npc2);
        panel.add(npc3);
        panel.add(_step);
        panel.add(_new);

        return panel;
    }

    //add the center grid to the screen
    public void setCenter(NewBoardPanel grid){
        this.center = grid;
    }

    //add the actionlisteners to the buttons
    //actionlistener implemented in the controller class
    void npc1Listener(ActionListener listener){
        this.npc1.addActionListener(listener);
    }
    void npc2Listener(ActionListener listener){
        this.npc2.addActionListener(listener);
    }
    void npc3Listener(ActionListener listener){
        this.npc3.addActionListener(listener);
    }
    void _stepListener(ActionListener listener){
        this._step.addActionListener(listener);
    }
    void _newListener(ActionListener listener){
        this._new.addActionListener(listener);
    }
}
