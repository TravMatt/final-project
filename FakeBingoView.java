import javax.swing.*; //Needed for Swing Classes
import java.awt.*;    //Needed for BorderLayout Classes
import java.awt.event.*;
@SuppressWarnings("serial")
public class FakeBingoView extends JFrame{

    private final int WINDOW_WIDTH  = 400;
    private final int WINDOW_HEIGHT = 300;
    //Since center panel is filled by data from Controller, it will be set by setCenter() when controller constructor called
    private Cell center;
    private JButton _step;
    private JButton _new;

    //Constructor
    public FakeBingoView(BingoModel model){

        System.out.println("FakeBingoView Constructor called");

        //set frame defaults
        setTitle("Border Layout");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //setLayout(new BorderLayout());

        this.center = model.playerList[0].board.cellHolder[0][0];

        //create Top, bottom panels
        //Set Top grid== "B I N G O"
        //Set Bottom == [NPC 1][NPC 2][NPC 3][Step][New Game]
        //setting center panel visible
        System.out.println("Setting center panel visible (called in BingoView)");
        add(this.center);
        pack();
    }
}
