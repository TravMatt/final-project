import javax.swing.*;


public class BingoMain {
    //Bingo App is a fully-functional game of Bingo, played by the player and 3 Non-Player Characters
    //BingoMain is responsible for initializing the three main parts of the app and starting the GUI
    //Has been created with a Model View Controller (MVC) architecture to manage complexity

    public static void main(String[] args) {
        System.out.println("BingoMain main() called");
        BingoModel      model       = new BingoModel("Player");
        BingoView       view        = new BingoView(model);
        BingoController controller  = new BingoController(model, view);

        //let the games begin!
        view.setVisible(true);
    }

}
