import java.awt.*;    //Needed for BorderLayout Classes
import javax.swing.*; //Needed for Swing Classes
import java.awt.event.*;
/*
 * This is the Controller of the Bingo Program: responsible for 
 *Controller-Objects:
 *A controller object acts as an intermediary between an application’s view objects and its model objects. Controller objects are thus a
 *conduit through which view objects learn about changes in model objects and vice versa.
 *
 *Controller objects can also perform setup and coordinating tasks for an application and manage the life cycles of other objects.
 *
 *Communication: A controller object interprets user actions made in view objects and communicates new or changed data to the model layer.
 *When model objects change, a controller object communicates that new model data to the view objects so that they can display it.
*/

public class FakeBingoController{

    //constructor
    public FakeBingoController(BingoModel model, FakeBingoView view){
        System.out.println("FakeBingoController Constructor called");

        /*
        //add actionlisteners to the view
        view.npc1Listener(new NPC1Popup());
        view.npc2Listener(new NPC2Popup());
        view.npc3Listener(new NPC3Popup());
        view._stepListener(new _stepAction());
        view._newListener(new _newAction());

        //add players grid to the GUI
        NewBoardPanel grid = model.getGrid();
        view.setCenter(grid);
        view.setVisible(true);
        */
    }

    class NPC1Popup implements ActionListener{
        public void actionPerformed(ActionEvent e){
            //code to popup the npc1 board
        }
    }

    class NPC2Popup implements ActionListener{
        public void actionPerformed(ActionEvent e){
            //code to popup the npc1 board
        }
    }

    class NPC3Popup implements ActionListener{
        public void actionPerformed(ActionEvent e){
            //code to popup the npc1 board
        }
    }

    class _stepAction implements ActionListener{
        public void actionPerformed(ActionEvent e){
            //code to popup the npc1 board
        }
    }

    class _newAction implements ActionListener{
        public void actionPerformed(ActionEvent e){
            //code to popup the npc1 board
        }
    }
}
