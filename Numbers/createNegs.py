import Image

for i in range(75):
    base = str(i) + ".jpg"
    print "accessing file: {0}".format(base)
    background = Image.open(base)
    overlay = Image.open("overlay.jpg")

    background = background.convert("RGBA")
    overlay = overlay.convert("RGBA")

    new_img = Image.blend(background, overlay, 0.5)
    new_file = "not" + base
    print "saving file: {0}".format(new_file)
    new_img.save(new_file, "jpg")
