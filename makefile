#
# A simple makefile for compiling three java classes
#

# define a makefile variable for the java compiler
#
JCC = javac

# define a makefile variable for compilation flags
# the -g flag compiles with debugging information
#
JFLAGS = -g

# typing 'make' will invoke the first target entry in the makefile 
# (the default one in this case)
#
default: BingoMain.class BingoModel.class BingoView.class BingoController.class Player.class  Cell.class NewBoardPanel.class

# this target entry builds the Average class
# the Average.class file is dependent on the Average.java file
# and the rule associated with this entry gives the command to create it
#
BingoMain.class: BingoMain.java
		$(JCC) $(JFLAGS) BingoMain.java

BingoModel.class: BingoModel.java Cell.class Player.class NewBoardPanel.class
		$(JCC) $(JFLAGS) BingoModel.java

NewBoardPanel.class: NewBoardPanel.java
		$(JCC) $(JFLAGS) NewBoardPanel.java

#NumberEngine.class: NumberEngine.java		$(JCC) $(JFLAGS) NumberEngine.java


Cell.class: Cell.java
		$(JCC) $(JFLAGS) Cell.java

BingoView.class: BingoView.java BingoModel.class
		$(JCC) $(JFLAGS) BingoView.java

BingoController.class: BingoController.java BingoView.class BingoModel.class
		$(JCC) $(JFLAGS) BingoController.java

Player.class: Player.java
		$(JCC) $(JFLAGS) Player.java

# To start over from scratch, type 'make clean'.  
# Removes all .class files, so that the next make rebuilds them
#
clean: 
		$(RM) *.class
