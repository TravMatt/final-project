import javax.swing.*;

//NonPlayerCharacter class implements fields and methods necessary to create player objects: subclass of Player Class
public class NonPlayerCharacter extends Player{

    //Constructor for the Player, NonPlayerCharacter subclasses
    public NonPlayerCharacter(){
        super();
        System.out.println("NPC being created");
    }

    //method to set object name must be implemented, either passed (if Player = user) or generated automatically
    //since User.setName() takes an argument, this should not be called
    public void setName(){
        String name = "NPC" + Integer.toString(getPlayerCount());
        this.name = name;
        setPlayerCount();        
    }

    //Set user name
    public void setName(String name){
        ;
    }

}
