import java.awt.*;    //Needed for BorderLayout Classes
import javax.swing.*; //Needed for Swing Classes
import java.awt.event.*;
import java.lang.StringBuilder;

/*
 *
 * This is the Controller of the Bingo Program: responsible for 
 *Controller-Objects:
 *A controller object acts as an intermediary between an application’s view objects and its model objects. Controller objects are thus a
 *conduit through which view objects learn about changes in model objects and vice versa.
 *
 *Controller objects can also perform setup and coordinating tasks for an application and manage the life cycles of other objects.
 *
 *Communication: A controller object interprets user actions made in view objects and communicates new or changed data to the model layer.
 *When model objects change, a controller object communicates that new model data to the view objects so that they can display it.
*/

public class BingoController{

    BingoView view;
    BingoModel model;
    //constructor
    public BingoController(BingoModel model, BingoView view){
        System.out.println("BingoController Constructor called");
        this.view  = view;
        this.model = model;

        //add actionlisteners to the view
        view.npc1Listener( new NPC1Popup());
        view.npc2Listener( new NPC2Popup());
        view.npc3Listener( new NPC3Popup());
        view._stepListener(new _stepAction());
        view._newListener( new _newAction());
        
        //add players grid to the GUI
        NewBoardPanel grid = model.getUserBoard();
        view.setCenter(grid);
        view.setVisible(true);
    }

    class NPC1Popup implements ActionListener{
        public void actionPerformed(ActionEvent e){
            //code to popup the npc1 board
            NPCView popup = new NPCView(model.getNPC1Board());
        }
    }

    class NPC2Popup implements ActionListener{
        public void actionPerformed(ActionEvent e){
            //code to popup the npc2 board
            NPCView popup = new NPCView(model.getNPC2Board());
        }
    }

    class NPC3Popup implements ActionListener{
        public void actionPerformed(ActionEvent e){
            //code to popup the npc3 board
            NPCView popup = new NPCView(model.getNPC3Board());
        }
    }

    class _stepAction implements ActionListener{
        public void actionPerformed(ActionEvent e){
            //code to execute the _step action
            boolean _winningStatus = model._callNumber();
            if(model.trackTurn < 75){
                if((_winningStatus == false)){
                    //I Found this to take away from the game too much: much better to just print out number
                    //JOptionPane.showMessageDialog(view, "The number " + model.numberCalled + " was called");
                    System.out.println("The number " + model.numberCalled + " was called.");
                }
                else{
                    declareWinners();
                }
            }
            else{
                JOptionPane.showMessageDialog(view, "No further numbers to call. All players lose");
                model.getNewBoards();
            }
        }
    }

    class _newAction implements ActionListener{
        public void actionPerformed(ActionEvent e){
            //code to execute the _new action
            model.getNewBoards();
            JOptionPane.showMessageDialog(view, "The game has been restarted");
        }
    }

    public void declareWinners(){
        StringBuilder msg = new StringBuilder();
        for(String name: model.winners){
            msg.append(name + " ");
        }
        if(model.winners.size() == 1){
            msg.append("has ");
        }
        else{
            msg.append("have ");
        }
        JOptionPane.showMessageDialog(view, msg + "won the Bingo game");
        model.getNewBoards();
    }

    //responsible for the pop-up box that will show the NPC's board
    @SuppressWarnings("serial")
    class NPCView extends JFrame{

        private final int WINDOW_WIDTH  = 800;
        private final int WINDOW_HEIGHT = 800;
        private NewBoardPanel board;

        //Constructor
        public NPCView(NewBoardPanel board){

            System.out.println("NPCView Constructor called");

            //set frame defaults
            setTitle("NPC View");
            //setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            this.setPreferredSize(new Dimension(500,500));
            this.board = board;
            //add panels to pane
            add(this.board);
            pack();
            this.setVisible(true);
        }
    }//END NPCView class
}
