import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.*;
import java.util.Random;

public class NumberEngine{
    /*This Class controls the generation of random numbers, suitable for creation of game boards and the iteration of the game*/
    //Class is static because there should be no need to instantiate, will be returning similar values every time

    /*The numbers in the columns of a Bingo ticket are selected at random and printed according to the range:
     *B column: are from 1 to 15
     *I column: 16 and 30
     *N column (that contains four numbers and the free (blank) space) between 31 and 45
     *G column between 46 and 60
     *O column: 61 and 75 */
    static int[] B = {1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15};
    static int[] I = {16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30};
    static int[] N = {31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45};
    static int[] G = {46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60};
    static int[] O = {61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75};
    static int[] completeNumbers = {1,  2,  3,  4,  5,  6,  7,  8,  9,  10, 11, 12, 13, 14, 15,
                                    16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,  
                                    31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45,
                                    46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 
                                    61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75};


    // Implementing Fisher–Yates shuffle
    static void shuffleArray(int[] ar){
        Random rnd = new Random();
        for (int i = ar.length - 1; i > 0; i--){
            int index = rnd.nextInt(i + 1);
            // Simple swap
            int a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
    }

    //a method to concatenate arrays together
    private static int[] concat(int[] arr1, int[] arr2, int[] arr3, int[] arr4, int[] arr5){
        int[] result = new int[25];
        System.arraycopy(arr1, 0, result,  0, 5);
        System.arraycopy(arr2, 0, result,  5, 5);
        System.arraycopy(arr3, 0, result, 10, 5);
        System.arraycopy(arr4, 0, result, 15, 5);
        System.arraycopy(arr5, 0, result, 20, 5);

        return result;
    }
        
    //takes & shuffles each respective column, concatenates all columns into one array & returns said array
    public static int[] genBoard(){
        System.out.println("genBoard being called");
        int[] colB = B; 
        int[] colI = I;
        int[] colN = N;
        int[] colG = G;
        int[] colO = O;

        shuffleArray(colB);
        shuffleArray(colI);
        shuffleArray(colN);
        shuffleArray(colG);
        shuffleArray(colO);
        int[] randomized = new int[75];
        //since gridLayout adds cells sequentially, this returns #'s in appropriate order: first of every column, second..
        for (int i = 0, j = 0; j < randomized.length; ++i) {
                randomized[j++] = colB[i];
                randomized[j++] = colI[i];
                randomized[j++] = colN[i];
                randomized[j++] = colG[i];
                randomized[j++] = colO[i];
        }           

        return randomized;
    }

    //generates the numbers necessary to iter through game 
    public static int[] genNums(){
        System.out.println("genNums being called");
        int[] numbers = completeNumbers;
        shuffleArray(numbers);
        return numbers;
    } 

    //testing the class
    public static void main(String[] args){
        System.out.println("calling genNums(): " + Arrays.toString( genNums() ));
        System.out.println("calling genBoard(): " + Arrays.toString( genBoard() ));

    }

    //only use to is make code in .genBoard() more terse: takes the shuffled array, returns the first 5 numbers
    public static int[] portion(int[] array){
        return Arrays.copyOfRange(array, 0, 5);
    }
}
